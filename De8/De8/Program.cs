﻿using De8;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Validate validation = new Validate();
int choice;

Service service = new Service();
service.Init();
do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xuất danh sách GV ngành UDPM");
    Console.WriteLine("4. Sắp xếp đội tượng theo ngành");
    Console.WriteLine("0. Thoát");
    choice = validation.InputIntegerInRange("Chọn chức năng: ", 0, 4);
    switch (choice)
    {
        case 1:
            service.AddTeacher();
            break;
        case 2:
            service.ShowTeacher();
            break;
        case 3:
            service.ShowTeacherUDPMs();
            break;
        case 4:
            service.SortByNganh();
            break;
        default:
            break;
    }
} while (choice > 0 && choice <= 4);
Console.ReadKey();
