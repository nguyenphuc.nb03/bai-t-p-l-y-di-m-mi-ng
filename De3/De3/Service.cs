﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De3
{
    internal class Service
    {
        public List<MayTinh> ListMayTinhs= new List<MayTinh>();
        Validate validate= new Validate();

        public string TuTangMa()
        {
            int sum = ListMayTinhs.Count;
            int Id = 1;

            if (sum > 0)
            {
                Id = int.Parse(ListMayTinhs[sum - 1].ID) + 1;
            }

            return Id.ToString();
        }

        public void AddMayTinh()
        {
            while (true)
            {
                string name;
                while (true)
                {
                    name = validate.InputString("Tên Máy Tính: ", "^[A-Za-z0-9\\s]+$");
                    if (validate.CheckNameExist(ListMayTinhs, name))
                    {
                        break;
                    }
                }
                string id= TuTangMa();
                float trongLuong = validate.InputFloat("Nhập trọng lượng", 0, 100);
                ListMayTinhs.Add(new MayTinh(id, name, trongLuong));

                Console.WriteLine("Bạn có muốn nhập tiếp không: ");
                if (!validate.CheckInputYN())
                {
                    return;
                }
            }
        }

        public void ShowMayTinh()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -15}{2, -5}", "Id", "Tên", "Trọng Lượng"));
            foreach(MayTinh mayTinh in ListMayTinhs)
            {
                mayTinh.InThongTin();
            }
        }

        public void DeleteById()
        {
            int flag = 0;
            string id = validate.InputString("Nhập mã máy tính", "^[A-Za-z0-9\\s]+$");
            for(int i= 0; i< ListMayTinhs.Count; i++)
            {
                if (ListMayTinhs[i].ID.Equals(id))
                {
                    flag = 1;
                    ListMayTinhs.Remove(ListMayTinhs[i]);
                }
            }
            if(flag == 0)
            {
                Console.WriteLine("Không tìm thấy máy tính");
            } else
            {
                Console.WriteLine("Xóa thành công");
            }
        }

        public void FindByName()
        {
            string name = validate.InputString("Tên máy tính", "^[A-Za-z0-9\\s]+$");
            foreach(MayTinh mayTinh in ListMayTinhs)
            {
                if (mayTinh.Ten.ToUpper().Contains(name.ToUpper()))
                {
                    Console.WriteLine(mayTinh.Ten+" Trọng lượng: "+ mayTinh.TrongLuong);
                }
            }
        }
        public void Init()
        {
            ListMayTinhs.Add(new MayTinh("1", "Dell", 1.5F));
            ListMayTinhs.Add(new MayTinh("2", "Lenovo", 1.5F));
            ListMayTinhs.Add(new MayTinh("3", "Asus", 1.5F));

        }

    }
}
